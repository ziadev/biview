/* ***** BEGIN COPYRIGHT BLOCK *****
Copyright (c) 2020 Zia Consulting, Inc., All Rights Reserved
NOTICE:  All information contained herein is, and remains the property of Zia Consulting, Inc.
The intellectual and technical concepts contained herein are proprietary to Zia Consulting, Inc. and may be
covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
copyright law. Dissemination of this information or reproduction of this material is strictly
forbidden unless prior written permission is obtained from Zia Consulting, Inc.

The copyright notice above does not evidence any actual or intended publication or disclosure
of  this source code, which includes information that is confidential and/or proprietary of Zia Consulting, Inc.

ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, OR PUBLIC DISPLAY OF OR
THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF ZIA CONSULTING, INC. IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.

THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, IN WHOLE OR IN PART.

THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
***** END COPYRIGHT BLOCK ***** */

import React from "react";

class ColumnFilterBox extends React.Component {
  constructor(props) {
    super(props);
    this.handleFilterColumnsChange = this.handleFilterColumnsChange.bind(this);
  }

  handleFilterColumnsChange(columnName, status) {
    this.props.onFilterColumnsChange(columnName, status);
  }

  render() {

    const allColumns = [
        "Batch ID",
        "Batch Name",
        "Current User",
        "Encryption Algorithm",
        "Executed Modules",
        "Executing Server",
        "Local Folders",
        "Batch Instance Priority",
        "Server IP",
        "Batch Status",
        "UNC Subfolder",
        "Review Operator Username",
        "Validation Operator Username",
        "Batch Class ID",
        "Batch Class Name",
        "Batch Class Description",
        "Custom Column 1",
        "Custom Column 2",
        "Custom Column 3",
        "Custom Column 4",
        "Creation Date",
        "Last Modified",
        "Is Remote Batch"
      ];

    const filters = allColumns.map(column => {
      const currentStatus = this.props.filterColumns.includes(column);
      return (
        <ColumnFilter
          key={column}
          onFilterColumnsChange={this.handleFilterColumnsChange}
          columnName={column}
          status={currentStatus}
        />
      );
    });

    return <div
      style={{paddingTop: "20px", paddingBottom:"20px"}} className="ColumnFilterBox-grid">{filters}</div>;
  }
}

class ColumnFilter extends React.Component {
  constructor(props) {
    super(props);
    this.handleStatusChange = this.handleStatusChange.bind(this);
  }

  handleStatusChange(e) {
    this.props.onFilterColumnsChange(this.props.columnName, e.target.checked);
  }

  render() {
    return (
      <form style={{marginTop: "-4px"}} className="ColumnFilter">
        <div>
          <label
            className="ColumnFilter-label"
            style={
              this.props.status
                ? { fontWeight: "bold" }
                : { fontWeight: "normal" }
            }
          >
            <input
              className="ColumnFilter-input"
              type="checkbox"
              defaultChecked={this.props.status}
              onChange={this.handleStatusChange}
            />
            {" " + this.props.columnName}
          </label>
        </div>
      </form>
    );
  }
}

export default ColumnFilterBox;
