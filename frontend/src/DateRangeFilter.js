/* ***** BEGIN COPYRIGHT BLOCK *****
Copyright (c) 2020 Zia Consulting, Inc., All Rights Reserved
NOTICE:  All information contained herein is, and remains the property of Zia Consulting, Inc.
The intellectual and technical concepts contained herein are proprietary to Zia Consulting, Inc. and may be
covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
copyright law. Dissemination of this information or reproduction of this material is strictly
forbidden unless prior written permission is obtained from Zia Consulting, Inc.

The copyright notice above does not evidence any actual or intended publication or disclosure
of  this source code, which includes information that is confidential and/or proprietary of Zia Consulting, Inc.

ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, OR PUBLIC DISPLAY OF OR
THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF ZIA CONSULTING, INC. IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.

THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, IN WHOLE OR IN PART.

THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
***** END COPYRIGHT BLOCK ***** */

import React from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import * as moment from 'moment-timezone';

class DateRangeFilter extends React.Component {
  constructor(props) {
    super(props);

    this.handleStartDateChanged = this.handleStartDateChanged.bind(this);
    this.handleEndDateChanged = this.handleEndDateChanged.bind(this);
  }

  handleStartDateChanged(moment) {
    this.props.onChange({
      startDate: moment,
      endDate: this.props.filter ? this.props.filter.value.endDate : null
    });
  }
  handleEndDateChanged(moment) {
    this.props.onChange({
      startDate: this.props.filter ? this.props.filter.value.startDate : null,
      endDate: moment
    });
  }

  render() {
    return (
      <div className="DateRangeFilter">
        <DateRangeButton
          handleSelectedDateRangeChanged={this.handleStartDateChanged}
          isStart={true}
          selectedDate={
            this.props.filter ? this.props.filter.value.startDate : null
          }
	  timeZone={this.props.timeZone}
        />
        <DateRangeButton
          handleSelectedDateRangeChanged={this.handleEndDateChanged}
          isStart={false}
          selectedDate={
            this.props.filter ? this.props.filter.value.endDate : null
          }
         timeZone={this.props.timeZone}
        />
      </div>
    );
  }
}


class DateRangeButton extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false,
      tempDate:
        this.props.selectedDate == null ? new Date() : this.props.selectedDate
    };

    this.toggleCalendar = this.toggleCalendar.bind(this);
    this.clearFilter = this.clearFilter.bind(this);
    this.handleSelectedDateRangeChanged = this.handleSelectedDateRangeChanged.bind(
      this
    );
  }

  clearFilter() {
    this.setState({ tempDate: null });
    this.props.handleSelectedDateRangeChanged(null);
  }

  handleSelectedDateRangeChanged(date) {
    // Update this after every selection to ensure the data within the calendar is updated
    this.setState({ tempDate: date });

    //Convert it to a moment, and update the timezone to the one the user is currentl using
    var momentDate = moment(date).tz(this.props.timeZone, true);

    // Only close after a time is selected
    if (
      this.state.tempDate != null &&
      (moment(this.state.tempDate).hour() !== momentDate.hour() ||
        moment(this.state.tempDate).minute() !== momentDate.minute())
    ) {
      //Pass it back up the chain
      this.props.handleSelectedDateRangeChanged(momentDate);
      this.toggleCalendar();
    }
  }

  toggleCalendar(e) {
    e && e.preventDefault();
    this.setState({ isOpen: !this.state.isOpen });
  }

  render() {
    return (
      <div>
        <button
          className={
            this.props.selectedDate == null
              ? "DateRangeButton"
              : "DateRangeButton-filtering"
          }
          onClick={this.toggleCalendar}
        >
          {this.props.isStart ? "Start" : "End"}
        </button>
        {this.state.isOpen && (
          <DatePicker
            selected={this.state.tempDate}
            onChange={this.handleSelectedDateRangeChanged}
            onClickOutside={this.toggleCalendar}
            peekNextMonth
            showMonthDropdown
            showYearDropdown
            dropdownMode="select"
            showTimeSelect
            withPortal
            inline
          />
        )}
        {this.props.selectedDate != null && (
          <DateRangeFilterClearButton onClick={this.clearFilter} />
        )}
      </div>
    );
  }
}

class DateRangeFilterClearButton extends React.Component {
  render() {
    return (
      <button
        className="DateRangeFilterClearButton"
        onClick={this.props.onClick}
      >
        X
      </button>
    );
  }
}

export default DateRangeFilter;
