/* ***** BEGIN COPYRIGHT BLOCK *****
Copyright (c) 2020 Zia Consulting, Inc., All Rights Reserved
NOTICE:  All information contained herein is, and remains the property of Zia Consulting, Inc.
The intellectual and technical concepts contained herein are proprietary to Zia Consulting, Inc. and may be
covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
copyright law. Dissemination of this information or reproduction of this material is strictly
forbidden unless prior written permission is obtained from Zia Consulting, Inc.

The copyright notice above does not evidence any actual or intended publication or disclosure
of  this source code, which includes information that is confidential and/or proprietary of Zia Consulting, Inc.

ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, OR PUBLIC DISPLAY OF OR
THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF ZIA CONSULTING, INC. IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.

THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, IN WHOLE OR IN PART.

THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
***** END COPYRIGHT BLOCK ***** */

import React, { Component } from "react";
import "./App.css";
import LoadingPlaceholder from "./LoadingPlaceholder";
import ErrorPlaceholder from "./ErrorPlaceholder";
import BITable from "./BITable";
import ColumnFilterBox from "./ColumnFilterBox";
import ColumnFilterHeader from "./ColumnFilterHeader";
import Logo from "./resources/Zia_Logo.png";


class App extends Component {
  constructor(props) {
    super(props);

    //Setup default state
    this.state = {
      batches: [],
      isLoading: false,
      error: null,
      serverTimeZone: Intl.DateTimeFormat().resolvedOptions().timeZone,
      selectedTimeZone: Intl.DateTimeFormat().resolvedOptions().timeZone,
      filterColumns: [
        "Batch ID",
        "Batch Name",
        "Batch Instance Priority",
        "Batch Status",
        "Batch Class ID",
        "Batch Class Description",
        "Creation Date",
        "Last Modified"
      ]
    };

    //Bind functions that we'll pass to children
    this.handleFilterColumnsChange = this.handleFilterColumnsChange.bind(this);
    this.fetchBatches = this.fetchBatches.bind(this);
    this.fetchTimeZone = this.fetchTimeZone.bind(this);
    this.fetchAll = this.fetchAll.bind(this);
    this.refreshBatches = this.refreshBatches.bind(this);
    this.updateTimeZone = this.updateTimeZone.bind(this);
    this.columns = [];
  }

  componentDidMount() {
    //First thing we do is load info from the server
    this.fetchAll();
  }

  //The user changed which columns they would like to filter on. Update state and cascade down
  handleFilterColumnsChange(columnName, status) {
    var filterCols = this.state.filterColumns.slice();

    if (status && !filterCols.includes(columnName)) {
      filterCols.push(columnName);
    }

    if (!status) {
      filterCols = filterCols.filter(column => {
        return column !== columnName;
      });
    }

    this.setState({ filterColumns: filterCols });
  }

  //Returns a Promise with an array of batches
  fetchBatches() {
    return fetch("./api/batchInstance").then(response => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error("Unable to load batches from Ephesoft.");
      }
    });
  }

  //Returns a Promise with a string of the IANA time zone
  fetchTimeZone() {
    return fetch("./api/timezone").then(response => {
      if (response.ok) {
        return response.text();
      } else {
        throw new Error("Unable to get time zone from the server.");
      }
    });
  }

  fetchAll() {
    //Show our loading screen.
    this.setState({ isLoading: true });

    //Start both API calls to the server
    Promise.all([this.fetchBatches(), this.fetchTimeZone()])
      //Update the state with the responses from the server, turn off "loading"
      .then(([serverBatches, timeZone]) =>
        this.setState({ batches: serverBatches, serverTimeZone: timeZone, isLoading: false })
      )
      //Handle whatever went wrong
      .catch(error => this.setState({ error: error, isLoading: false }));

  }

  //Refresh the batches by loading them from the server.
  refreshBatches() {
    //Show our loading screen.
    this.setState({ isLoading: true });

    //Start both API calls to the server
    this.fetchBatches()
      //Update the state with the responses from the server, turn off "loading"
      .then(serverBatches => this.setState({ batches: serverBatches, isLoading: false }))
      //Handle whatever went wrong
      .catch(error => this.setState({ error: error, isLoading: false }));
  }

  //Update the user's selected timezone for display.
  updateTimeZone(timeZone) {
    this.setState({ selectedTimeZone: timeZone });
  }

  render() {
    //Explode the current state for easier access
    const { batches, isLoading, error, serverTimeZone, selectedTimeZone, filterColumns } = this.state;
    var columns = []
    var batchObject = this.state.batches[0];
    for (var column in batchObject){
      if (batchObject.hasOwnProperty(column)){
        columns.push(column)
      }
    }
    this.columns = columns
    console.log(columns)



    //Check if we're in an error state, and display it.
    if (error) {
      return (
        <div className="App">
          <h1>Batch Instance Viewer</h1>
          <ErrorPlaceholder error={error} />
        </div>
      );
    }

    //Check if we're in still loading, and display it.
    if (isLoading) {
      return (
        <div className="App">
          <h1>Batch Instance Viewer</h1>
          <LoadingPlaceholder />
        </div>
      );
    }

    //No errors, not loading, let's display the good stuff
    return (
      <div className="App">
      <div className="Header">
        <img className="Logo" src={Logo} />
        <h1 className ="Title">Batch Instance Viewer</h1>
        <div className="ColumnFilterBox">
          <ColumnFilterHeader
            onRefreshClick={this.refreshBatches}
            serverTimeZone={serverTimeZone}
            selectedTimeZone={selectedTimeZone}
            onTimeZoneChange={this.updateTimeZone}
            />
            <ColumnFilterBox onFilterColumnsChange={this.handleFilterColumnsChange} filterColumns={filterColumns} columns={this.columns}/>
        </div>
      </div>
        <BITable filterColumns={filterColumns} timeZone={selectedTimeZone} batches={batches} />
      </div>
    );
  }
}

export default App;
