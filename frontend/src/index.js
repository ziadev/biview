/* ***** BEGIN COPYRIGHT BLOCK *****
Copyright (c) 2019 Zia Consulting, Inc., All Rights Reserved
NOTICE:  All information contained herein is, and remains the property of Zia Consulting, Inc.
The intellectual and technical concepts contained herein are proprietary to Zia Consulting, Inc. and may be
covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
copyright law. Dissemination of this information or reproduction of this material is strictly
forbidden unless prior written permission is obtained from Zia Consulting, Inc.

The copyright notice above does not evidence any actual or intended publication or disclosure
of  this source code, which includes information that is confidential and/or proprietary of Zia Consulting, Inc.

ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, OR PUBLIC DISPLAY OF OR
THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF ZIA CONSULTING, INC. IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.

THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, IN WHOLE OR IN PART.

THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
***** END COPYRIGHT BLOCK ***** */

import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

ReactDOM.render(<App />, document.getElementById('root'));
