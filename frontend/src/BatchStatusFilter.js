/* ***** BEGIN COPYRIGHT BLOCK *****
Copyright (c) 2020 Zia Consulting, Inc., All Rights Reserved
NOTICE:  All information contained herein is, and remains the property of Zia Consulting, Inc.
The intellectual and technical concepts contained herein are proprietary to Zia Consulting, Inc. and may be
covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
copyright law. Dissemination of this information or reproduction of this material is strictly
forbidden unless prior written permission is obtained from Zia Consulting, Inc.

The copyright notice above does not evidence any actual or intended publication or disclosure
of  this source code, which includes information that is confidential and/or proprietary of Zia Consulting, Inc.

ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, OR PUBLIC DISPLAY OF OR
THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF ZIA CONSULTING, INC. IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.

THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, IN WHOLE OR IN PART.

THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
***** END COPYRIGHT BLOCK ***** */

import React from "react";
import StatefulMultiSelect from "@khanacademy/react-multi-select";

class BatchStatusFilter extends React.Component {
  constructor(props) {
    super(props);
    this.handleSelectedBatchStatusChanged = this.handleSelectedBatchStatusChanged.bind(
      this
    );
  }

  handleSelectedBatchStatusChanged(selectedStatuses) {
    this.props.onChange(selectedStatuses);
  }

  render() {
    const allStatuses = [
      { label: "New", value: "NEW" },
      { label: "Ready", value: "READY" },
      { label: "Error", value: "ERROR" },
      { label: "Finished", value: "FINISHED" },
      { label: "Running", value: "RUNNING" },
      { label: "Ready for Review", value: "READY_FOR_REVIEW" },
      { label: "Ready for Validation", value: "READY_FOR_VALIDATION" },
      { label: "Restarted", value: "RESTARTED" },
      { label: "Transferred", value: "TRANSFERRED" },
      { label: "Deleted", value: "DELETED" },
      { label: "Restart in Progress", value: "RESTART_IN_PROGRESS" },
      { label: "Remote", value: "REMOTE" },
      { label: "Created", value: "CREATED" },
      { label: "Waiting to Restart", value: "WAITING_TO_RESTART" }
    ];

    return (
      <div className="BatchStatusFilter">
        <StatefulMultiSelect
          options={allStatuses}
          selected={
            this.props.filter
              ? this.props.filter.value
              : allStatuses.map(status => status.value)
          }
          onSelectedChanged={this.handleSelectedBatchStatusChanged}
          overrideStrings={{
            selectSomeItems: "Select Statuses",
            allItemsAreSelected: "All",
            selectAll: "Select All",
            search: "Search"
          }}
        />
      </div>
    );
  }
}

export default BatchStatusFilter;
