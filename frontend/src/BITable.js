/* ***** BEGIN COPYRIGHT BLOCK *****
Copyright (c) 2020 Zia Consulting, Inc., All Rights Reserved
NOTICE:  All information contained herein is, and remains the property of Zia Consulting, Inc.
The intellectual and technical concepts contained herein are proprietary to Zia Consulting, Inc. and may be
covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
copyright law. Dissemination of this information or reproduction of this material is strictly
forbidden unless prior written permission is obtained from Zia Consulting, Inc.

The copyright notice above does not evidence any actual or intended publication or disclosure
of  this source code, which includes information that is confidential and/or proprietary of Zia Consulting, Inc.

ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, OR PUBLIC DISPLAY OF OR
THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF ZIA CONSULTING, INC. IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.

THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, IN WHOLE OR IN PART.

THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
***** END COPYRIGHT BLOCK ***** */

import React from "react";
import ReactTable from "react-table";
import "react-table/react-table.css";
import BatchStatusFilter from "./BatchStatusFilter";
import DateRangeFilter from "./DateRangeFilter";
import * as moment from 'moment-timezone';
import { ExportToCsv } from 'export-to-csv';

class BITable extends React.Component {
  constructor(props) {
    super(props);
    this.sortBatchId = this.sortBatchId.bind(this);
    this.sortBatchClassId = this.sortBatchClassId.bind(this);
    this.sortHex = this.sortHex.bind(this);
    this.filterDate = this.filterDate.bind(this);
    this.getAndDownloadTableData = this.getAndDownloadTableData.bind(this);
    this.renderedData = [];
    this.allColumns = [
      {
        id: "Batch ID",
        Header: "Batch ID",
        accessor: "batchId",
        sortMethod: this.sortBatchId,
        filterMethod: (filter, row) => String(row[filter.id]) === filter.value
      },
      {
        id: "Batch Name",
        Header: "Batch Name",
        accessor: "batchName"
      },
      {
        id: "Current User",
        Header: "Current User",
        accessor: "currentUser"
      },
      {
        id: "Encryption Algorithm",
        Header: "Encryption Algorithm",
        accessor: "encryptionAlgorithm"
      },
      {
        id: "Executed Modules",
        Header: "Executed Modules",
        accessor: "executedModules"
      },
      {
        id: "Executing Server",
        Header: "Executing Server",
        accessor: "executingServer"
      },
      {
        id: "Local Folders",
        Header: "Local Folders",
        accessor: "localFolders"
      },
      {
        id: "Batch Instance Priority",
        Header: "Priority",
        accessor: "batchPriority",
        Cell: props => <span className="number">{props.value}</span>,
        filterMethod: (filter, row) => row[filter.id] == filter.value
      },
      {
        id: "Server IP",
        Header: "Server IP",
        accessor: "serverIP"
      },
      {
        id: "Batch Status",
        Header: "Batch Status",
        accessor: "batchStatus",
        filterMethod: (filter, row) => filter.value.includes(row[filter.id]),
        Filter: ({ filter, onChange }) => (
          <BatchStatusFilter filter={filter} onChange={onChange} />
        )
      },
      {
        id: "UNC Subfolder",
        Header: "UNC Subfolder",
        accessor: "uncSubfolder"
      },
      {
        id: "Review Operator Username",
        Header: "Review Operator Username",
        accessor: "reviewOperatorUsername"
      },
      {
        id: "Validation Operator Username",
        Header: "Validation Operator Username",
        accessor: "validationOperatorUsername"
      },
      {
        id: "Batch Class ID",
        Header: "Batch Class ID",
        accessor: "batchClassId",
        sortMethod: this.sortBatchClassId,
        filterMethod: (filter, row) => String(row[filter.id]) === filter.value
      },
      {
        id: "Batch Class Name",
        Header: "Batch Class Name",
        accessor: "batchClassName"
      },
      {
        id: "Batch Class Description",
        Header: "Batch Class Description",
        accessor: "batchClassDescription"
      },
      {
        id: "Custom Column 1",
        Header: "Custom Column 1",
        accessor: "customColumn1"
      },
      {
        id: "Custom Column 2",
        Header: "Custom Column 2",
        accessor: "customColumn2"
      },
      {
        id: "Custom Column 3",
        Header: "Custom Column 3",
        accessor: "customColumn3"
      },
      {
        id: "Custom Column 4",
        Header: "Custom Column 4",
        accessor: "customColumn4"
      },
      {
        id: "Creation Date",
        Header: "Creation Date",
        accessor: "creationDate",
        Cell: props => {
        	const date = moment(props.value);
        	return date.tz(this.props.timeZone).format("ll LTS");
        },
        filterMethod: this.filterDate,
        Filter: ({ filter, onChange }) => (
          <DateRangeFilter filter={filter} onChange={onChange} timeZone={this.props.timeZone}/>
        )
      },
      {
        id: "Last Modified",
        Header: "Last Modified",
        accessor: "lastModified",
        Cell: props => {
        	const date = moment(props.value);
        	return date.tz(this.props.timeZone).format("ll LTS");
        },
        filterMethod: this.filterDate,
        Filter: ({ filter, onChange }) => (
          <DateRangeFilter filter={filter} onChange={onChange} timeZone={this.props.timeZone}/>
        )
      },
      {
        id: "Is Remote Batch",
        Header: "Is Remote Batch",
        accessor: "remote"
      }
    ];
  }

  sortBatchId(a, b) {
    return this.sortHex("BI", a, b);
  }

  sortBatchClassId(a, b) {
    return this.sortHex("BC", a, b);
  }

  sortHex(prefix, a, b) {
    //Kinda hacky, but kinda clever
    return parseInt(a.replace(prefix, "0x")) - parseInt(b.replace(prefix, "0x"));
  }

  filterDate(filter, row) {
    //Get the start and end date selected by the user (may be missing/null)
    const { startDate, endDate } = filter.value;

    //Get the actual date for this row that we're checking
    const rowDate = row[filter.id];
    const rowMoment = moment(rowDate);

    //Assume we keep it, and possibly invalidate this assumption by checking filter
    var keep = true;

    //Don't keep this row if it's before the user-selected start date (if the user set one)
    if (startDate) {
      if (rowMoment.isBefore(startDate)) {
        keep = false;
      }
    }

    //Don't keep this row if it's after the user-selected end date (if the user set one)
    if (endDate) {
      if (rowMoment.isAfter(endDate)) {
        keep = false;
      }
    }

    return keep;
  }

  getAndDownloadTableData(){
    var selectedColumns = this.props.filterColumns
    var formatedColumns = []
    selectedColumns.forEach(selectedColumnObject => {
      this.allColumns.forEach(columnObject => {
        if (columnObject.id == selectedColumnObject){
          console.log(columnObject.accessor)
          formatedColumns.push(columnObject.accessor)
        }
    })
  });

    var preparedData = this.props.batches.map(item => {
      // get only what we want from each row object
      var filteredRow = {};
      formatedColumns.forEach(element => {
        if ( element == "creationDate" || element == "lastModified"){
        	const date = moment(item[element].toString());
          var data = date.tz(this.props.timeZone).format("ll LTS");
        } else {
          var data = item[element]
        }
        filteredRow[element] = data
      })
      return filteredRow;
    });
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalSeparator: '.',
      showLabels: true,
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: true
    };

    const csvExporter = new ExportToCsv(options);
    csvExporter.generateCsv(preparedData);
  }

  render() {


    //Filter columns by which ones are actually selected by the user.
    var columns = this.allColumns.filter(column =>
      this.props.filterColumns.includes(column.id)
    );

    return (
      <div className="BITable">
        <ReactTable
          filterable
          defaultFilterMethod={(filter, row) =>
            row[filter.id].toLowerCase().includes(filter.value.toLowerCase())
          }
          data={this.props.batches}
          defaultSorted={[{id:"batchId",desc: false}]}
          columns={columns}
          getTheadThProps={() => {
            return {
              style: {
                outline: 0
              }
            };
          }}
          //style the header row
          getTheadTrProps={() => {
            return {
              style: {
              fontWeight: "bold",
              fontSize: "12px",
              backgroundColor: "#1E264C",
              color: "white",
              height: "50px",
              paddingTop: "10px"
             }
            };
          }}
          //Ensure that there are alternating colors between rows
          className="-striped -highlight "
          ref={(r) => { this.renderedData = r; }}
        >
          {(state, makeTable, instance) => {
            let recordsInfoText = "";
            const { filtered, pageRows, pageSize, sortedData, page } = state;
            if (sortedData && sortedData.length > 0) {
              let isFiltered = filtered.length > 0;
              let totalRecords = sortedData.length;
              let recordsCountFrom = page * pageSize + 1;
              let recordsCountTo = recordsCountFrom + pageRows.length - 1;
              recordsInfoText = `${recordsCountFrom}-${recordsCountTo} of ${totalRecords} records`;
            } else recordsInfoText = "No records";
            return (
              <div align="center">
                {makeTable()}
                <br/>
                <span>{recordsInfoText}</span>
                <br/><br/>
                <button className="DownloadResultsButton" onClick={this.getAndDownloadTableData}>Download</button>
              </div>
            );
          }}
        </ReactTable>
        </div>
    );
  }
}

export default BITable;
