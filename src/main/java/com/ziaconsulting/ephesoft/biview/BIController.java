/* ***** BEGIN COPYRIGHT BLOCK *****
Copyright (c) 2019 Zia Consulting, Inc., All Rights Reserved
NOTICE:  All information contained herein is, and remains the property of Zia Consulting, Inc.
The intellectual and technical concepts contained herein are proprietary to Zia Consulting, Inc. and may be
covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
copyright law. Dissemination of this information or reproduction of this material is strictly
forbidden unless prior written permission is obtained from Zia Consulting, Inc.

The copyright notice above does not evidence any actual or intended publication or disclosure
of  this source code, which includes information that is confidential and/or proprietary of Zia Consulting, Inc.

ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, OR PUBLIC DISPLAY OF OR
THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF ZIA CONSULTING, INC. IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.

THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, IN WHOLE OR IN PART.

THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
***** END COPYRIGHT BLOCK ***** */

package com.ziaconsulting.ephesoft.biview;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BIController {

	public static final Logger logger = LoggerFactory.getLogger(BIController.class);

	private static final String EPHESOFT_QUERY = "select bi.identifier, "
			+ "bi.batch_name, "
			+ "bi.curr_user, "
			+ "bi.encryption_algorithm, "
			+ "bi.executed_modules, "
			+ "bi.executing_server, "
			+ "bi.is_remote, "
			+ "bi.local_folder, "
			+ "bi.batch_priority, "
			+ "bi.server_ip, "
			+ "bi.batch_status, "
			+ "bi.unc_subfolder, "
			+ "bi.validation_operator_user_name, "
			+ "bi.review_operator_user_name, "
			+ "bi.custom_column1, "
			+ "bi.custom_column2, "
			+ "bi.custom_column3, "
			+ "bi.custom_column4, "
			+ "bi.creation_date, "
			+ "bi.last_modified, "
			+ "bc.identifier as batch_class_id, "
			+ "bc.batch_class_name, "
			+ "bc.batch_class_description "
			+ "from batch_instance bi "
			+ "JOIN batch_class bc ON bi.batch_class_id = bc.id;";

	private static final String REPORT_QUERY = "select identifier, "
			+ "batch_name, "
			+ "curr_user, "
			+ "encryption_algorithm, "
			+ "executed_modules, "
			+ "executing_server, "
			+ "is_remote, "
			+ "local_folder, "
			+ "batch_priority, "
			+ "server_ip, "
			+ "batch_status, "
			+ "unc_subfolder, "
			+ "validation_operator_user_name, "
			+ "review_operator_user_name, "
			+ "'' as custom_column1, "
			+ "'' as custom_column2, "
			+ "'' as custom_column3, "
			+ "'' as custom_column4, "
			+ "creation_date, "
			+ "last_modified, "
			+ "batch_class_id, "
			+ "batch_class_name, "
			+ "batch_class_description "
			+ "from batch_instance;";



	@Autowired
	DataSource ephesoftDataSource;

	@Autowired
	DataSource reportDataSource;

	@Autowired
	DataSource reportArchiveDataSource;

	@GetMapping("/api/timezone")
	public String getTimezone() {
		//Returns the IANA timezone string from the server sid. Example: "America/New_York"
		return ZoneId.systemDefault().getId();
	}

	@GetMapping("/api/batchInstance")
	public Set<BatchInfo> batchInstance() {

		SortedSet<BatchInfo> allBatches = new TreeSet<BatchInfo>();

		//Add the archive batches
		allBatches.addAll(getBatchInstancesFromDB(reportArchiveDataSource, REPORT_QUERY));

		//Add the report batches
		allBatches.addAll(getBatchInstancesFromDB(reportDataSource, REPORT_QUERY));

		//Add the Ephesoft batches
		allBatches.addAll(getBatchInstancesFromDB(ephesoftDataSource, EPHESOFT_QUERY));


		return allBatches;
	}

	private SortedSet<BatchInfo> getBatchInstancesFromDB(DataSource dataSource, String queryString) {
		SortedSet<BatchInfo> batchInfoList = new TreeSet<>();
		try (Connection connection = dataSource.getConnection()) {
			PreparedStatement query = connection.prepareStatement(queryString);
			ResultSet resultSet = query.executeQuery();
			while (resultSet.next()) {
				BatchInfo bi  = convertRowToBatchInfo(resultSet);
				if(bi!=null) {
				batchInfoList.add(bi);}
			}

		} catch (Exception e) {
			logger.error("Unable to query the database.", e);
			throw new RuntimeException("Unable to query the database",e);
		}
		return batchInfoList;
	}

	private BatchInfo convertRowToBatchInfo(ResultSet resultSet ) {
		try {
			String batchId = resultSet.getString("identifier");
			String batchName = resultSet.getString("batch_name");
			String currentUser = resultSet.getString("curr_user");
			String encryptionAlgorithm = resultSet.getString("encryption_algorithm");
			String executedModules = resultSet.getString("executed_modules");
			String executingServer = resultSet.getString("executing_server");
			boolean isRemote = resultSet.getBoolean("is_remote");
			String localFolders = resultSet.getString("local_folder");
			int batchPriority = resultSet.getInt("batch_priority");
			String serverIP = resultSet.getString("server_ip");
			String batchStatus = resultSet.getString("batch_status");
			String uncSubfolder = resultSet.getString("unc_subfolder");
			String validationOperatorUsername = resultSet.getString("validation_operator_user_name");
			String reviewOperatorUsername = resultSet.getString("review_operator_user_name");
			String batchClassId = resultSet.getString("batch_class_id");
			String batchClassName = resultSet.getString("batch_class_name");
			String batchClassDescription = resultSet.getString("batch_class_description");
			String customColumn1 = resultSet.getString("custom_column1");
			String customColumn2 = resultSet.getString("custom_column1");
			String customColumn3 = resultSet.getString("custom_column1");
			String customColumn4 = resultSet.getString("custom_column1");
			Instant creationDate = resultSet.getTimestamp("creation_date").toInstant();
			Instant lastModified = resultSet.getTimestamp("last_modified").toInstant();
			return new BatchInfo(batchId, batchName, currentUser, encryptionAlgorithm, executedModules,
					executingServer, isRemote, localFolders, batchPriority, serverIP, batchStatus, uncSubfolder,
					validationOperatorUsername, reviewOperatorUsername, batchClassId, batchClassName,
					batchClassDescription, customColumn1, customColumn2, customColumn3,
					customColumn4, creationDate, lastModified);

		} catch (Exception e) {
			logger.error("Unable to process one of the batch instance rows. Skipping this row.", e);
			return null;
		}
	}
}
