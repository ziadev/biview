/* ***** BEGIN COPYRIGHT BLOCK *****
Copyright (c) 2019 Zia Consulting, Inc., All Rights Reserved
NOTICE:  All information contained herein is, and remains the property of Zia Consulting, Inc.
The intellectual and technical concepts contained herein are proprietary to Zia Consulting, Inc. and may be
covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
copyright law. Dissemination of this information or reproduction of this material is strictly
forbidden unless prior written permission is obtained from Zia Consulting, Inc.

The copyright notice above does not evidence any actual or intended publication or disclosure
of  this source code, which includes information that is confidential and/or proprietary of Zia Consulting, Inc.

ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, OR PUBLIC DISPLAY OF OR
THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF ZIA CONSULTING, INC. IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.

THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, IN WHOLE OR IN PART.

THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
***** END COPYRIGHT BLOCK ***** */

package com.ziaconsulting.ephesoft.biview;

import java.time.Instant;

public class BatchInfo implements Comparable<BatchInfo> {

	private final String batchId;
	private final int hexId;
	private final String batchName;
	private final String currentUser;
	private final String encryptionAlgorithm;
	private final String executedModules;
	private final String executingServer;
	private final boolean isRemote;
	private final String localFolders;
	private final int batchPriority;
	private final String serverIP;
	private final String batchStatus;
	private final String uncSubfolder;
	private final String validationOperatorUsername;
	private final String reviewOperatorUsername;
	private final String batchClassId;
	private final String batchClassName;
	private final String batchClassDescription;
	private final String customColumn1;
	private final String customColumn2;
	private final String customColumn3;
	private final String customColumn4;
	private final Instant creationDate;
	private final Instant lastModified;

	public BatchInfo(String batchId, String batchName, String currentUser, String encryptionAlgorithm,
			String executedModules, String executingServer, boolean isRemote, String localFolders, int batchPriority,
			String serverIP, String batchStatus, String uncSubfolder, String validationOperatorUsername,
			String reviewOperatorUsername, String batchClassId, String batchClassName, String batchClassDescription,
			String customColumn1, String customColumn2, String customColumn3, String customColumn4,
			Instant creationDate, Instant lastModified) {
		this.batchId = batchId == null ? "" : batchId;
		this.hexId = Integer.parseInt(this.batchId.replace("BI", ""), 16);
		this.batchName = batchName == null ? "" : batchName;
		this.currentUser = currentUser == null ? "" : currentUser;
		this.encryptionAlgorithm = encryptionAlgorithm == null ? "" : encryptionAlgorithm;
		this.executedModules = executedModules == null ? "" : executedModules;
		this.executingServer = executingServer == null ? "" : executingServer;
		this.isRemote = isRemote;
		this.localFolders = localFolders == null ? "" : localFolders;
		this.batchPriority = batchPriority;
		this.serverIP = serverIP == null ? "" : serverIP;
		this.batchStatus = batchStatus == null ? "" : batchStatus;
		this.uncSubfolder = uncSubfolder == null ? "" : uncSubfolder;
		this.validationOperatorUsername = validationOperatorUsername == null ? "" : validationOperatorUsername;
		this.reviewOperatorUsername = reviewOperatorUsername == null ? "" : reviewOperatorUsername;
		this.batchClassId = batchClassId == null ? "" : batchClassId;
		this.batchClassName = batchClassName == null ? "" : batchClassName;
		this.batchClassDescription = batchClassDescription == null ? "" : batchClassDescription;
		this.customColumn1 = customColumn1 == null ? "" : customColumn1;
		this.customColumn2 = customColumn2 == null ? "" : customColumn2;
		this.customColumn3 = customColumn3 == null ? "" : customColumn3;
		this.customColumn4 = customColumn4 == null ? "" : customColumn4;
		this.creationDate = creationDate;
		this.lastModified = lastModified;

	}

	public String getBatchId() {
		return batchId;
	}

	public int getHexId() {
		return hexId;
	}

	public String getBatchName() {
		return batchName;
	}

	public String getCurrentUser() {
		return currentUser;
	}

	public String getEncryptionAlgorithm() {
		return encryptionAlgorithm;
	}

	public String getExecutedModules() {
		return executedModules;
	}

	public String getExecutingServer() {
		return executingServer;
	}

	public boolean isRemote() {
		return isRemote;
	}

	public String getLocalFolders() {
		return localFolders;
	}

	public int getBatchPriority() {
		return batchPriority;
	}

	public String getServerIP() {
		return serverIP;
	}

	public String getBatchStatus() {
		return batchStatus;
	}

	public String getUncSubfolder() {
		return uncSubfolder;
	}

	public String getValidationOperatorUsername() {
		return validationOperatorUsername;
	}

	public String getReviewOperatorUsername() {
		return reviewOperatorUsername;
	}

	public String getBatchClassId() {
		return batchClassId;
	}

	public String getBatchClassName() {
		return batchClassName;
	}

	public String getBatchClassDescription() {
		return batchClassDescription;
	}

	public String getCustomColumn1() {
		return customColumn1;
	}

	public String getCustomColumn2() {
		return customColumn2;
	}

	public String getCustomColumn3() {
		return customColumn3;
	}

	public String getCustomColumn4() {
		return customColumn4;
	}

	public Instant getCreationDate() {
		return creationDate;
	}

	public Instant getLastModified() {
		return lastModified;
	}

	@Override
	public int compareTo(BatchInfo other) {
		return this.hexId - other.hexId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((batchId == null) ? 0 : batchId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BatchInfo other = (BatchInfo) obj;
		if (batchId == null) {
			if (other.batchId != null)
				return false;
		} else if (!batchId.equals(other.batchId))
			return false;
		return true;
	}

}
