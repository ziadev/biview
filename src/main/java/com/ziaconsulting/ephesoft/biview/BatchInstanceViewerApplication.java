/* ***** BEGIN COPYRIGHT BLOCK *****
Copyright (c) 2019 Zia Consulting, Inc., All Rights Reserved
NOTICE:  All information contained herein is, and remains the property of Zia Consulting, Inc.
The intellectual and technical concepts contained herein are proprietary to Zia Consulting, Inc. and may be
covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
copyright law. Dissemination of this information or reproduction of this material is strictly
forbidden unless prior written permission is obtained from Zia Consulting, Inc.

The copyright notice above does not evidence any actual or intended publication or disclosure
of  this source code, which includes information that is confidential and/or proprietary of Zia Consulting, Inc.

ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, OR PUBLIC DISPLAY OF OR
THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF ZIA CONSULTING, INC. IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.

THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, IN WHOLE OR IN PART.

THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
***** END COPYRIGHT BLOCK ***** */

package com.ziaconsulting.ephesoft.biview;

import javax.sql.DataSource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;

@SpringBootApplication
public class BatchInstanceViewerApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(BatchInstanceViewerApplication.class, args);
	}

	@Bean(name = "ephesoftDataSource")
	public DataSource ephesoftDataSource(){
		JndiDataSourceLookup dataSourceLookup = new JndiDataSourceLookup();
		  DataSource dataSource = dataSourceLookup.getDataSource("jdbc/ephesoft");
		  return dataSource;
	}

	@Bean(name = "reportDataSource")
	public DataSource reportDataSource(){
		JndiDataSourceLookup dataSourceLookup = new JndiDataSourceLookup();
		  DataSource dataSource = dataSourceLookup.getDataSource("jdbc/reports");
		  return dataSource;
	}

	@Bean(name = "reportArchiveDataSource")
	public DataSource reportArchiveDataSource(){
		JndiDataSourceLookup dataSourceLookup = new JndiDataSourceLookup();
		  DataSource dataSource = dataSourceLookup.getDataSource("jdbc/reports_archive");
		  return dataSource;
	}


}
