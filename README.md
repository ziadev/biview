# **Ephesoft Batch Instance Viewer - What problems does it solve?**

#### **Centralization of Batch Instance Data**

The Zia Ephesoft Transact Batch Instance Viewer—BI Viewer—centralizes all the information that pertains to Batch Instances. Outside of BI Viewer, there is really no place (other than the databases themselves) to query data about a batch's status, creation date, server IP, executing server, et cetera in the same interface. 

#### **Troubleshooting, Analytics, & Auditing**

The BI Viewer's countless filtering and sorting methods make it an excellent place to troubleshoot and investigate issues that may arise in batch processing.

For example, say an Ephesoft environment has issues related to what seems like *random* batches hitting the Error state. You've looked in the Batch Instance Manager for solutions, but the batches that hit error seem to truly be random. BI Viewer can shed some light on the issue. You can filter batches to only show batches within the last month with the status Error. By enabling columns, patterns or matching values in the field emerge. Once you enable the Executing Server, you notice all the batches in Error are running on the same server. That server must have an issue causing batches to fail.

### **What is it?**

The Zia Ephesoft Transact Batch Instance Viewer—BI Viewer—is a web application that is designed to help Ephesoft Transact super users report and dig up information pertaining to every batch instance that has been run through an Ephesoft instance.

Ephesoft Transact moves completed batches quickly to the reporting database. While those batches can be seen in the Reports user interface they are moved to an archive database around 30 days after the batch instances have completed. Often we have customers that desire to see the current and historical data in one place.

The BI Viewer is lightweight with only one interface; The BI table. Upon accessing the BI Viewer for the first time, you will see the following screen:

**![img](https://lh5.googleusercontent.com/k7uG2uL217hU3_EYxGaKt7Nu9NoPmCWFD5f3S1RGFhU00QR_qiyvZfuauSjxb5T4d_mXV-5UNpPZXRPmY3RrAZWHBblmt--gLY2rDw1a7IKs9HABhoNYT7fbexh4w3rxSRvQQVqK)**

There are only two components to the interface:

1. The checkboxes that show and hide columns within the table
2. The batch instances in a table

#### **Checkbox Components**

**![img](https://lh5.googleusercontent.com/ql7evX4sNx0vpO5QYPXjDJAjxymFvMHFZWLgZaM7wqIzqx5_XtZsAABFxM0LF69VEmVX7lIMm-HS1Zp0feMHbQdAm6yIbE8htZEjzl4sz8yhBio2i8hPWnLxzHHcdPaLJZKB5Qdd)**

The Checkbox components are a list of columns that you would like to query. Checking a checkbox will immediately update the table with the proper information for each shown batch instance.

#### **Batch Instance Table**

**![img](https://lh3.googleusercontent.com/ujiRAXv2a3ti0UrO4UOiE0g2vGMrQz31orAZbmyAWHerqhpaMgJ_S2XmZX5RsxzvPCjzfa4rzWGPSW5CvgIPsdVeGZfFtW7ieXEvJBPrVIPHYM6lQRMvWFbQFBEA21Pw_96XhCQx)**

This is where the results of the query are shown. The results are paginated by default by 20 rows. This can be changed on the table footer with the rows dropdown. By default, the results are ordered ascending by batch instance ID, however, this can be changed by clicking on any of the columns headers to sort by ascending or descending values. Additionally, the Bi Viewer allows for a few more criteria in the column headers:

1. **Batch Status**Because there is a fixed number of batch statuses, the BI Viewer tool has a drop-down menu under the column header for selecting which batch status' to display.
2. **Date (Creation, Last-Modified)**Users are also able to create a date and time range for the Creation Date and Last Modified fields with the start and end buttons below each column header. Clicking the buttons will show a calendar month view alongside a time picker. Once selected, users can click the ‘X’ button to clear that date and time.
   **![img](https://lh6.googleusercontent.com/1cxaIWYtg0KZ220WfyP0n_rtFDaVJ8AauTUtIHHXLTJFX8gMVwbpA6POldj2jph_XxCGzIPn5pimbdvCFbcP6WqGT1gccfFXwrBJUrsGeFnHaHF1KeElTEiu5n0l8wwrJIAoRMIK)**![img](https://lh3.googleusercontent.com/Hb6aN-IZJ-Djr2n10l4sIwMSciwEbponyM2lj3aWB_hlBNU4RHpQz5XNxCvT579wiq7sRvYVRBnIAkX4rC6PlWNTtmZGn1nQSPtGXB2HVJfUq463dkXpZoUzOtR4xa1vFRXhFL4m)
3. **Text Matching (all other fields)**Outside of the above criteria, each other header has a text field that will accept any text for a plain-text match. For example, if a user is searching for a specific batch instance, they can enter the BIID in the field under the Batch ID column header:
   **![img](https://lh5.googleusercontent.com/ZrmBmpxYmzjKtoApBm5w2WMy5ayqizrPGa51LzviEOzhP5WYaoRY-9ENujVloDcUMt57kCGN4Z2pjk4r_Fa9oltGE1SbnQjHYI1rb45L3XLUIgG5aG8lEJfmTB5nalTom62jT08u)**Or if a user is looking only for batch instances for a specific batch class, they can enter the BCID under the Batch Class ID column header:
   **![img](https://lh4.googleusercontent.com/rXinVKEkr4Ej6Lp70rum9aarQFxoymCgN6-5mmBXVwOuH5x0j_3QiVSWgZcfUy-gHhpfZF_UbGUzCazZgTqjPU0Itg6YZVSQo5e-Q77bl5xU9t_FM6e5LeI8bLQBdvqfjjmILxTB)**

#### **Additional Features**

1. **List of Results**

Shown at the bottom of the page (below the table) is a number of returned Batch instances. This can be helpful if users need to simply view how many batches match a given criteria created with the filters on the table. For example, there are X number of batches in Error state within a certain time period.



1. **CSV Download**

Also shown below the table is a Download Button. This button downloads every page of the filtered results (current table data) to a CSV file for later parsing or better use in Excel or other spreadsheet processing applications.

**![img](https://lh5.googleusercontent.com/mmxD959XYBHjhFVrE-xeLQwsTGvPiWWLlFAqQHuMECIoaOC0sQe2Futgp1JbH3ptoM3Uo4otrO3nAAwmNYei3bvt9m0sdO_Ek5XcwnhCGmcnQ_EUWnIv4IyN_rYqAwDGQ31DlfgF)**

#### **Ephesoft Integration**

With a Java backend and a React driven front-end, the BI Viewer is designed to run on the same tomcat instance and port as Ephesoft.

# Usage and Development

This projects adds an additional web page alongside Ephesoft to display _all_ batch instance, including those which have been moved to the report or report_archive database. The user can search and filter to find their desired batch instance. 

This is a read-only view, and the user will not be able to change the batch instance data in any way.The application is deployed into the same Tomcat container as Ephesoft, and shares the same database pools. No database configuration knowledge is needed by the webapp.

#### Deployment

Option 1: Auto-deploy

1. Update Ephesoft/JavaAppServer/server.xml to auto deploy webapps (autoDeploy="true").
2. Restart Ephesoft so this configuration change takes effect.
3. Copy the biview.war file into Ephesoft/JavaAppServer/webapps/.
4. Tomcat will automatically deploy the web application.

Option 2: Manual deploy

1. Extract biview.war into Ephesoft/JavaAppServer/webapps/biview.
2. Restart Ephesoft to deploy the application.

#### Accessing the Application

The batch instance viewer webapp will now be available at <Tomcat_Root>/biview.

If your Ephesoft URL is usually http://localhost:8080/dcma/home.html, then batch instance viewer will be at http://localhost:8080/biview

If your Ephesoft URL is usually https://ephesoft.example.com/dcma/home.html, then batch instance viewer will be at https://ephesoft.example.com/biview

#### Setup Notes

##### Tools

* Maven
* NPM

##### Build the project

The project can be built using `mvn clean install`.

The output file will be available under target/biview.war.

The webapp can then be deployed following the instructions in the "Deployment" section.

##### Front-end development only

Building and deploying is a slow process. Fortunately, if you're only making front-end changes to the UI, there is a shortcut to iterate quickly:

1. Build and deploy the webapp as usual to make the API available.
2. Launch a terminal
3. `cd frontend`
4. `npm start`
5. This will launch a browser and connect to the webapp on port 3000. 
6. Any changes to the front-end are instantly compiled and displayed in this browser. 

When you're done making front-end changes, don't forget to rebuild and deploy the whole WAR so your changes are served by Tomcat. 